define([
  'store/EntryStore',
  'config',
  'store/promiseUtil',
  'rdfjson/namespaces',
  'dojo/node!babyparse',
], (EntryStore, config, promiseUtil, namespaces, babyparse) => {
  namespaces.add('org', 'http://www.w3.org/ns/org#');

  console.log('csvTest code loaded');
  let filePath;
  let contextId;

  if (process.argv.length > 4) {
    contextId = process.argv[3];
    filePath = process.argv[4];
  }
  if (contextId == null || filePath == null) {
    console.log('Required parameters: contextid, relativefilepath');
    return;
  }
  const parsed = babyparse.parseFiles(`../import/${filePath}`, {
    header: true,
  });
  const rows = parsed.data;
  console.log(`Loaded ${rows.length} rows from ${filePath}`);

  const entrystore = new EntryStore(config.repository);

  entrystore.getAuth().login(config.user, config.password).then(() => {
    console.log('Authenticated');
    const context = entrystore.getContextById(contextId);

    entrystore.newSolrQuery().rdfType('org:Organization').context(context).limit(1).list()
      .getEntries().then((orgArr) => {
        const orgURI = orgArr[0].getResourceURI();
      // Add 10 entries to context.
        const srows = rows.slice(0, 4);
        promiseUtil.forEach(srows, row => entrystore.newSolrQuery().context(context).rdfType('foaf:Person')
          .uriProperty('foaf:mbox', `mailto:${row.email}`).limit(1).list().getEntries()
          .then((personArr) => {
            if (personArr.length > 0) {
              console.log(`Updating person: ${row.given} ${row.family}`);
              const pmd = personArr[0].getMetadata();
              const stmts = pmd.find(null, 'foaf:givenName');
              stmts[0].setValue(`${stmts[0].getValue()}2`);
              return personArr[0].commitMetadata();
            }
            console.log(`Importing person: ${row.given} ${row.family}`);
            const ne = context.newNamedEntry()
                .add('rdf:type', 'foaf:Person')
                .addL('foaf:givenName', row.given)
                .addL('foaf:familyName', row.family)
                .add('foaf:mbox', `mailto:${row.email}`);

            const md = ne.getMetadata();
            const membershipStmt = md.add(ne.getResourceURI(), 'org:hasMembership');
            const blankId = membershipStmt.getValue();
            md.add(blankId, 'rdf:type', 'org:Membership');
            md.add(blankId, 'org:organization', orgURI);

            return ne.commit();
          }));
      }, (err) => {
        console.log(err.stack);
      });
  }, (err) => {
    console.log(`Failed authentication ${err}`);
  });
});
