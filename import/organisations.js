define([
    'store/EntryStore',
    'config',
    'store/promiseUtil',
    'rdfjson/namespaces',
    'dojo/node!babyparse',
    'utf8/utf8',
], (EntryStore, config, promiseUtil, namespaces, babyparse) => {
    namespaces.add('org', 'http://www.w3.org/ns/org#');

    let contextId;
    let rdfType;
    let entryLimit;

    if (process.argv.length > 5) {
        contextId = process.argv[3];
        rdfType = parseInt(process.argv[4]);
        if (process.argv.length > 5) {
            entryLimit = parseInt(process.argv[5]);
        }
    }
    if (contextId == null || rdfType == null || entryLimit == null) {
        console.log('Required parameters: contextid, rdfType, entryLimit');
        console.log('rdfType 0 = debug, 1 = org:Organization, 2 = OrganizationalUnit, 3 = sites, 4 = persons');
        console.log('entryLimit 0 => all rows, > 0 => 0-limit');
        return;
    }

    const entrystore = new EntryStore(config.repository);

    entrystore.getAuth().login(config.user, config.password).then(() => {
        console.log('Authenticated');
        const context = entrystore.getContextById(contextId);
        var rows;

        switch (rdfType) {
            case 0:
                debugPrint(rows, entrystore, context);
                break;
            case 1:
                console.log('Indexing organisations');
                rows = parseFile("../import/organisations/organisations.csv", babyparse, entryLimit);
                return indexOrgs(rows, entrystore, context, promiseUtil);
                break;
            case 2:
                console.log('Indexing Units');
                rows = parseFile('../import/organisations/units.csv', babyparse, entryLimit);
                return indexUnits(rows, entrystore, context, promiseUtil, contextId);
                break;
            case 3:
                console.log('Indexing sites');
                rows = parseFile('../import/organisations/sites.csv', babyparse, entryLimit);
                return indexSites(rows, entrystore, context, promiseUtil, contextId);
                break;
            case 4:
                console.log('Indexing People');
                rows = parseFile('../import/organisations/persons.csv', babyparse, entryLimit);
                return indexPersons(rows, entrystore, context, promiseUtil, contextId);
                break;


        }
    }, (err) => {
        console.log(`Failed authentication ${err}`);
    });
});


/*
        Index Units
*/
function indexUnits(rows, entrystore, context, promiseUtil, contextId) {

    promiseUtil.forEach(rows, row => entrystore.newSolrQuery().context(context).rdfType('org:OrganizationalUnit')
        .literalProperty('http://goteborg.se/terms/responsibility', row.child_alias).limit(1).list().getEntries()
        .then((unitArr) => {
            if (row.parent_alias == null) {
                console.log('did you input the correct file?');
                return;
            } else {
                if (unitArr.length > 0) {
                    return updateUnit(row, context, 'org:OrganizationalUnit', entrystore, unitArr[0], contextId);
                }
                return addUnit(row, context, 'org:OrganizationalUnit', entrystore, contextId);
            }
        }));
}

function addUnit(row, context, type, entrystore, contextId) {
    console.log(`Importing ${type}: ${row.child_title}`);
    //Could use newLink with better uri-format
    const ne = context.newNamedEntry(row.child_alias)
        .add('rdf:type', type)
        .addL('http://goteborg.se/terms/responsibility', row.child_alias)
        .addL('skos:prefLabel', row.child_alias + ' ' + row.child_title);
    if (row.parent_alias !== 'NULL') {
        const parentURI = entrystore.getEntryURI(contextId, row.parent_alias);
        ne.add("org:subOrganizationOf", parentURI);
    }
    return ne.commit();
}

function updateUnit(row, context, type, entrystore, unit, contextId) {
    console.log(`Updating ${type}: ${row.child_title}`);
    const md = unit.getMetadata();
    const responsibility = md.find(null, 'http://goteborg.se/terms/responsibility');
    responsibility[0].setValue(`${row.child_alias}`);
    const label = md.find(null, 'skos:prefLabel');
    label[0].setValue(`${row.child_alias} ${row.child_title}`);
    const parentURI = entrystore.getEntryURI(contextId, row.parent_alias);
    md.findAndRemove(null, 'org:subOrganizationOf');
    if (row.parent_alias !== 'NULL') {
        unit.add("org:subOrganizationOf", parentURI);
    }
    return unit.commitMetadata();
}


/*
          Index Organisations
*/


function indexOrgs(rows, entrystore, context, promiseUtil) {
    promiseUtil.forEach(rows, row => entrystore.newSolrQuery().context(context).rdfType('org:Organization')
        .literalProperty('skos:prefLabel', `${row.Beskrivning}`).limit(1).list().getEntries()
        .then((orgArr) => {

            if (row.BegrVärde.toLowerCase().startsWith('b')) {
                // Can't perform updates yet
                /*if(orgArr.length > 0){        
                  return updateOrg(row, orgArr);                                                
                }*/
                return addOrg(row, context, 'org:Organization');
            }
        }));
}

function updateOrg(row, orgArr) {
    const pmd = orgArr[0].getMetadata();
    const stmts = pmd.find(null, 'skos:prefLabel');
    if (stmts[0].getValue() !== row.Beskrivning) {
        console.log(`Updating Organization: ${stmts[0].getValue()}`);
        stmts[0].setValue(row.Beskrivning);
        return orgArr[0].commitMetadata();
    }
}

function addOrg(row, context, type) {
    console.log(`Importing ${type}: ${row.Beskrivning}`);
    const ne = context.newNamedEntry()
        .add('rdf:type', type)
        .addL('http://goteborg.se/terms/responsibility', row.BegrVärde)
        .addL('skos:prefLabel', row.Beskrivning);
    return ne.commit();
}


/*
        Index sites 
*/

function indexSites(rows, entrystore, context, promiseUtil, contextId) {
    promiseUtil.forEach(rows, row => entrystore.newSolrQuery().context(context).rdfType('org:Site')
        .literalProperty('http://goteborg.se/terms/id', `${row.ID}`).limit(1).list().getEntries()
        .then((siteArr) => {
            if (row.Namn !== null) {
                if (siteArr.length > 0) {
                    return updateSite(row, context, 'org:Site', entrystore, siteArr[0], contextId);
                }
                return addSite(row, context, 'org:Site', entrystore, contextId);
            } else {
                consol.log("Could not find a name for this site")
            }
        }));
}

function addSite(row, context, type, entrystore, contextId) {
    console.log(`Importing ${type}: ${row.Namn}`);
    //Could use newLink with better uri-format
    const ne = context.newNamedEntry('site_' + row.ID)
        .add('rdf:type', type)
        .addL('skos:prefLabel', row.Namn)
        .addL('http://goteborg.se/terms/id', row.ID);
    const md = ne.getMetadata();
    const stmt = md.add(ne.getResourceURI(), 'org:siteAddress');
    const ml = stmt.getValue();
    md.add(ml, 'rdf:type', "http://www.w3.org/2006/vcard/ns#Address");
    md.addL(ml, 'vcard:locality', row.Postadress);
    md.addL(ml, "vcard:postal-code", row.Postnummer);
    md.addL(ml, 'vcard:street-address', row.Adress);
    return ne.commit().then(null, (err) => {
        console.log(err);
    });
}

function updateSite(row, context, type, entrystore, site, contextId) {
    console.log(`Updating ${type}: ${row.Namn}`);
    const md = site.getMetadata();
    const label = md.find(null, 'skos:prefLabel');
    label.setValue(row.Namn);
    md.findAndRemove(null, null, null);
    const stmt = md.add(site.getResourceURI(), 'org:siteAddress');
    const ml = stmt.getValue();
    md.add(ml, 'rdf:type', "http://www.w3.org/2006/vcard/ns#Address");
    md.addL(ml, 'vcard:locality', row.Postadress);
    md.addL(ml, "vcard:postal-code", row.Postnummer);
    md.addL(ml, 'vcard:street-address', row.Adress);
    return site.commitMetadata();
}

/*
        Index Persons 
*/

function indexPersons(rows, entrystore, context, promiseUtil, contextId) {
    promiseUtil.forEach(rows, row => entrystore.newSolrQuery().context(context).rdfType('foaf:Person')
        .literalProperty('http://goteborg.se/terms/id', `${row.ID}`).limit(1).list().getEntries()
        .then((personArr) => {
            if (personArr.length > 0) {
                return updatePerson(row, context, 'foaf:Person', entrystore, personArr[0], contextId);
            }
            return addPerson(row, context, 'foaf:Person', entrystore, contextId);
        }));
}

function addPerson(row, context, type, entrystore, contextId) {    
    console.log(`Importing person ${row.full_name} ${row.lastname}`)
    const ne = context.newNamedEntry('person_' + row.ID)
                .add('rdf:type', type)
                .addL('foaf:givenName', row.full_name)
                .addL('foaf:familyName', row.lastname)
                .addL('http://goteborg.se/terms/id', row.ID)
                .add('foaf:mbox', `mailto:${row.email}`);   
    return ne.commit().then(null, (err) => {
        console.log(err);
    });
}

function updatePerson(row, context, type, entrystore, person, contextId) {
    console.log(`Updating ${type}: ${row.full_name}  ${row.lastname}`);
    const md = person.getMetadata();
    const givenName = md.find(null, 'foaf:givenName');
    givenName[0].setValue(row.full_name);
    const familyName = md.find(null, 'foaf:familyName');
    familyName[0].setValue(row.lastname);
    const mail = md.find(null, 'foaf:mbox');
    mail[0].setValue(row.email);              
    return site.commitMetadata();
}

/*
        Index Roles 
*/

function indexRoles(rows, entrystore, context, promiseUtil, contextId) {
    promiseUtil.forEach(rows, row => entrystore.newSolrQuery().context(context).rdfType('org:role')
        .literalProperty('http://goteborg.se/terms/id', `${row.ID}`).limit(1).list().getEntries()
        .then((roleArr) => {
            if (roleArr.length > 0) {
                return updatePerson(row, context, 'org:role', entrystore, roleArr[0], contextId);
            }
            return addPerson(row, context, 'org:role', entrystore, contextId);
        }));
}

function addRole(row, context, type, entrystore, contextId) {    
    console.log(`Importing person ${row.full_name} ${row.lastname}`)
    const ne = context.newNamedEntry('person_' + row.ID)
                .add('rdf:type', type)
                .addL('foaf:givenName', row.full_name)
                .addL('foaf:familyName', row.lastname)
                .addL('http://goteborg.se/terms/id', row.ID)
                .add('foaf:mbox', `mailto:${row.email}`);   
    return ne.commit().then(null, (err) => {
        console.log(err);
    });
}

function updatePerson(row, context, type, entrystore, person, contextId) {
    console.log(`Updating ${type}: ${row.full_name}  ${row.lastname}`);
    const md = person.getMetadata();
    const givenName = md.find(null, 'foaf:givenName');
    givenName[0].setValue(row.full_name);
    const givenName = md.find(null, 'foaf:familyName');
    givenName[0].setValue(row.lastname);
    const givenName = md.find(null, 'foaf:mbox');
    givenName[0].setValue(row.email);              
    return site.commitMetadata();
}



function debugPrint(rows, entrystore, context) {
    entrystore.newSolrQuery().context(context).rdfType('org:OrganizationalUnit')
        .literalProperty('http://goteborg.se/terms/responsibility', `struktur`).limit(1).list().getEntries()
        .then((orgArr) => {
            console.log(orgArr[0].getResourceURI());
        });
}

function parseFile(filePath, babyparse, entryLimit) {
    console.log('parsing file: ' + filePath + '...');

    const parsed = babyparse.parseFiles(filePath, {
        header: true,
    }, (err) => {
        console.log(`Failed Parse ${err}`);
    });
    console.log('Parse successful');
    var out = parsed.data;
    if (entryLimit > 0) {
        out = out.slice(0, entryLimit);
    }    
    return out;
}