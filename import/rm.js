define([
    'store/EntryStore',
    'store/EntryStoreUtil',
    'config',
], (EntryStore, EntryStoreUtil, config) => {
    let ctxtid;
    let type;
    if (process.argv.length > 3) {
        ctxtid = process.argv[3];
    }



    if (process.argv.length > 4) {
        var typeId = parseInt(process.argv[4]);
        switch (typeId) {
            case 0:
                type = "*"
                break;
            case 1:
                type = "org:Organization";
                break;
            case 2:
                type = "org:OrganizationalUnit";
                break;
            case 3:
                type = "org:Site";
                break;
            case 4:
                type = "foaf:Person";
                break;
        }
        console.log('Removing ' + type);
    }

    if (ctxtid == null) {
        console.log('You need to provide the context id from where to remove stuff');
    } else if (type == null) {
        console.log('You need to provide a type to delete, 0 = everything, 1 = org:Organization, 2 = OrganizationalUnit, 3 = Sites, 4 = Persons');
    } else {
        const es = new EntryStore(config.repository);
        es.getAuth().login(config.user, config.password).then(() => {
            const toDelete = [];
            var esu = new EntryStoreUtil(es);
            esu.removeAll(es.newSolrQuery().title('*').context(es.getContextById(ctxtid)).rdfType(type).list());
        });
    }
});