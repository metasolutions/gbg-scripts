define([
  'config',
], (config) => {
  config.exportAsAPI = {
    port: 8182,
    people: {
      constraints: { 'rdf:type': ['foaf:Person'] },
      projection: {
        given: 'foaf:givenName',
        family: 'foaf:familyName',
        email: 'foaf:mbox',
      },
    },
  };
  require(['store-tools/exportapi/service']);
});
